import java.util.Scanner;

public class Kvur { //Ax^2 + Bx + C = 0
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("A = ");
        double A = in.nextDouble();

        System.out.print("B = ");
        double B = in.nextDouble();

        System.out.print("C = ");
        double C = in.nextDouble();

        double D = B*B - 4*A*C;

        if (D < 0) { //x^2 + x + 1 = 0
            System.out.println("Корней нет.");
        } else if (D == 0) { //3x^2 - 18x + 27 = 0 (x = 3)
            double x = -B / (2*A);
            System.out.println("x = " + x);
        } else { //x^2 + 17x - 18 = 0 (x1 = 1, x2 = -18)
            double x1 = (-B + Math.sqrt(D)) / (2*A);
            double x2 = (-B - Math.sqrt(D)) / (2*A);
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);
        }
    }
}
